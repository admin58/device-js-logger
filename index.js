const SerialPort = require('serialport')
const Readline = require('@serialport/parser-readline')

let comPorts = {}

function onDeviceAdded(device) {    
    try {
        const port = new SerialPort(device.path, e => {
            if (e) {
                console.error(e)
                console.warn(`unable to open com port ${device.path}`)
                delete comPorts[device.path]
            } else {
                console.log(`${device.path} opened!`)

                const parser = new Readline({ delimiter: '\r\n' })
                port.pipe(parser)
                parser.on('data', data => {
                    console.log(`(${device.path}): ${data}`)
                })
                port.on('close', () => {
                    console.log(`${device.path} was closed!`)
                    delete comPorts[device.path]
                })                
            }
        })
    } catch (e) { 
        console.error(e);
        console.warn(`unable to open com port ${device.path}`);
        delete comPorts[device.path]
    }
}

setInterval(() => {
    SerialPort.list()
    .then(info => {
        info.filter(v => v.vendorId == '1915' && v.productId == '776a')
            .forEach(v => {
                if (!comPorts.hasOwnProperty(v.path)) {
                    comPorts[v.path] = v
                    onDeviceAdded(v)
                }
            })
    }).catch(e => {
        console.error(e)
    })
}, 1000)